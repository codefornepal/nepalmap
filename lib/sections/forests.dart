import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/expandable_list.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:flutter/material.dart';

Widget getForests(Map data) {
  List<Widget> forests = [];

  if (data["area_has_data"] != null && data["area_has_data"] == true) {
    forests.addAll([
      new StatList(
        data["total_square_kilometres"],
        "number",
        title: "Total Land",
      ),
      new DistributionChart(
        data["forest_distribution"],
        title: "Forested and unforested land",
      ),
      new Divider(),
    ]);

    return new ExpandableList(
      title: "Forests and Land Use",
      children: forests,
    );
  }

  return Container();
}
