import 'package:NepalMap/components/expandable_list.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:flutter/material.dart';

Widget getDevelopment(Map data) {
  List<Widget> development = [];

  if (data["area_has_data"] != null && data["area_has_data"] == true) {
    development.addAll([
      new StatList(
        data["district_aid_amounts"],
        "dollar",
        title: "District-level Development Assistance",
      ),
      new StatList(
        data["district_aid_projects"],
        "number",
      ),
      new Divider(),
    ]);

    return new ExpandableList(
      title: "Development",
      children: development,
    );
  }

  return Container();
}
