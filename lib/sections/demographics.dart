import 'package:NepalMap/components/expandable_list.dart';
import 'package:NepalMap/subsections/demographics/age.dart';
import 'package:NepalMap/subsections/demographics/caste_and_ethnic.dart';
import 'package:NepalMap/subsections/demographics/citizenship.dart';
import 'package:NepalMap/subsections/demographics/disabled.dart';
import 'package:NepalMap/subsections/demographics/language.dart';
import 'package:NepalMap/subsections/demographics/life_expectancy_income.dart';
import 'package:NepalMap/subsections/demographics/population.dart';
import 'package:flutter/material.dart';

Widget getDemographics(Map data) {
  List<Widget> demographics = [];

  if (data["has_data"] != null && data["has_data"] == true) {
    demographics.addAll([
      new Population(data),
    ]);
    if (data["is_vdc"] != null && data["is_vdc"] == false) {
      demographics.addAll([
        new Age(data),
        new LifeExpectancyAndIncome(data),
        new Language(data),
        new Citizenship(data),
        new CasteAndEthnicGroup(data),
      ]);
    }
    demographics.add(new Disabled(data));

    return new ExpandableList(
      title: "Demographics",
      children: demographics,
    );
  }

  return new Container(
    margin: new EdgeInsets.only(top: 20.0),
    child: new Center(
      child: new Text("No data availble for this area."),
    ),
  );
}
