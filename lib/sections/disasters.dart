import 'package:NepalMap/components/expandable_list.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:flutter/material.dart';

Widget getDisasters(Map data) {
  List<Widget> disasters = [];

  if (data["area_has_data"] != null && data["area_has_data"] == true) {
    disasters.addAll([
      new StatList(
        data["flood_deaths"],
        "number",
        title: "Disaster-related deaths",
      ),
      new Divider(),
    ]);

    return new ExpandableList(
      title: "Disasters",
      children: disasters,
    );
  }

  return Container();
}
