import 'package:NepalMap/components/expandable_list.dart';
import 'package:NepalMap/subsections/business/registeration_decade.dart';
import 'package:NepalMap/subsections/business/registeration_era.dart';
import 'package:flutter/material.dart';

Widget getBusiness(Map data) {
  List<Widget> business = [];

  if (data["area_has_data"] != null && data["area_has_data"] == true) {
    business.addAll([
      new RegisterationDecade(data),
      new RegisterationEra(data),
    ]);

    return new ExpandableList(
      title: "Business",
      children: business,
    );
  }

  return new Container();
}
