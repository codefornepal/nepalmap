import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:flutter/material.dart';

Widget getElections(Map data) {
  List<Widget> elections = [];

  if (data["area_has_data"] != null && data["area_has_data"] == true) {
    elections.addAll([
      new StatList(
        data["total_voters"],
        "number",
        title: "Voters",
      ),
      new DistributionChart(
        data["voter_distribution"],
        pie: true,
        title: "Voters by sex",
      ),
      new Divider(),
      new StatList(
        data["total_electoral_bodies"],
        "number",
        title: "Local Electoral Bodies",
      ),
      new DistributionChart(
        data["local_electoral_bodies_distribution"],
        pie: true,
        title: "Local electoral bodies",
      ),
      new Divider(),
      new StatList(
        data["polling_places"],
        "number",
        title: "Polling Places and Registered Political Parties",
      ),
      new StatList(
        data["political_parties"],
        "number",
      ),
      new Divider(),
      new StatList(
        data["total_mayoralities"],
        "number",
        title: "Mayoral Seats by Party",
      ),
      new DistributionChart(
        data["mayoral_party_distribution"],
        title: "Mayoral seats held by party",
      ),
      new Divider(),
      new StatList(
        data["total_deputy_mayoralities"],
        "number",
        title: "Deputy Mayoral Seats by Party",
      ),
      new DistributionChart(
        data["deputy_mayoral_party_distribution"],
        title: "Deputy mayoral seats held by party",
      ),
      new Divider(),
    ]);

    return new ExpansionTile(
      title: new Text(
        "Elections",
        style: new TextStyle(fontSize: 20.0),
      ),
      initiallyExpanded: false,
      children: elections,
    );
  }

  return new Container();
}
