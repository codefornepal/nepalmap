import 'package:flutter/material.dart';
import 'package:NepalMap/containers/homepage.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'NepalMap',
      theme: new ThemeData(
        primaryColor: new Color(0xFFE00015),
      ),
      home: new HomePageContainer(),
    );
  }
}

class HomePageContainer extends StatelessWidget {
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new HomePage(),
    );
  }
}
