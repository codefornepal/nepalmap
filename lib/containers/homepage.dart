import 'dart:math';

import 'package:NepalMap/codeMappings.dart';
import 'package:NepalMap/components/customroute.dart';
import 'package:NepalMap/components/search_ui.dart';
import 'package:NepalMap/containers/profile.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  double headerHeight = 150.0, buttonHeight = 50.0;
  bool showTextField = false;

  AnimationController slideAnimationController;
  Animation<double> slideAnimation;

  AnimationController marginAnimationController;
  Animation<double> marginAnimation;

  @override
  initState() {
    super.initState();

    slideAnimationController = new AnimationController(
        duration: const Duration(milliseconds: 200), vsync: this);

    marginAnimationController = new AnimationController(
        duration: const Duration(milliseconds: 200), vsync: this);

    marginAnimation =
        new Tween(begin: 10.0, end: 0.0).animate(slideAnimationController)
          ..addListener(() {
            setState(() {});
          });

    slideAnimation =
        new Tween(begin: (headerHeight - buttonHeight / 2.0), end: 24.0)
            .animate(slideAnimationController)
              ..addStatusListener((status) {
                if (status == AnimationStatus.completed) {
                  showDialog(
                    context: context,
                    builder: (context) {
                      return new Container(
                        child: new SearchUI(),
                      );
                    },
                  ).whenComplete(() {
                    slideAnimationController.reverse();
                    marginAnimationController.reverse();
                  });
                }
              });
  }

  dispose() {
    slideAnimationController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return new Container(
      child: new Stack(
        children: <Widget>[
          new Container(
            color: Theme.of(context).primaryColor,
            height: headerHeight,
            child: new Center(
              child: new Text(
                "NepalMap",
                style: new TextStyle(
                  fontSize: 50.0,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          new GestureDetector(
            onTap: () {
              slideAnimationController.forward();
              marginAnimationController.forward();
            },
            child: new Container(
              child: new Card(
                elevation: 2.0,
                margin: new EdgeInsets.only(
                  top: slideAnimation.value,
                  left: marginAnimation.value,
                  right: marginAnimation.value,
                ),
                child: new Container(
                  height: 60.0 - marginAnimation.value,
                  alignment: Alignment.centerLeft,
                  padding: new EdgeInsets.symmetric(horizontal: 10.0),
                  child: new Row(
                    children: <Widget>[
                      new Container(
                        padding: new EdgeInsets.all(10.0),
                        child: new Icon(Icons.search),
                      ),
                      new Expanded(
                        child: new Text(
                          "Find a place in Nepal...",
                          style: new TextStyle(fontSize: 18.0),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          new HomeButton(
            "Feeling lucky!",
            top: headerHeight + buttonHeight,
            margin: 10.0,
            onPressed: () {
              int length = codeMappings.length;
              var random = new Random();
              int randomNumber = random.nextInt(length);
              String randomKey = codeMappings.keys.elementAt(randomNumber);

              Navigator.of(context).push(
                    new CustomRoute(
                      builder: (_) => new Profile(
                            code: randomKey,
                            name: codeMappings[randomKey],
                          ),
                    ),
                  );
            },
          ),
          new HomeButton(
            "Browse Nepal",
            top: headerHeight + buttonHeight * 2.5,
            margin: 10.0,
            onPressed: () => Navigator.of(context).push(
                  new CustomRoute(
                    builder: (_) => new Profile(
                          code: "country-NP",
                          name: "Nepal",
                        ),
                  ),
                ),
          ),
        ],
      ),
    );
  }
}

class HomeButton extends StatelessWidget {
  final String title;
  final double top;
  final double margin;
  final VoidCallback onPressed;

  HomeButton(
    this.title, {
    @required this.top,
    @required this.margin,
    this.onPressed,
  });

  Widget build(BuildContext context) {
    return new Container(
      margin: new EdgeInsets.only(top: top, left: margin, right: margin),
      child: Row(
        children: <Widget>[
          new Container(
            margin: new EdgeInsets.only(right: margin),
            child: new Text(
              "OR",
              style: new TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          new Expanded(
            child: new MaterialButton(
              onPressed: onPressed,
              color: Theme.of(context).primaryColor,
              child: new Container(
                padding: new EdgeInsets.symmetric(vertical: 15.0),
                child: new Text(
                  title,
                  style: new TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
