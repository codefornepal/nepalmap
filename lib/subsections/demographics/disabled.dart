import 'package:flutter/material.dart';

import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class Disabled extends StatelessWidget {
  final Map data;

  Disabled(this.data);

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          new SubSectionHeader("Disabled"),
          new StatList(
            data["percent_disabled"],
            "percentage",
          ),
          new DistributionChart(
            data["disability_ratio"],
            title: "Disability",
            pie: true,
          ),
          new Divider(),
        ],
      ),
    );
  }
}
