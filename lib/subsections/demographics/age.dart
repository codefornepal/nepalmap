import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class Age extends StatelessWidget {
  final Map data;

  Age(this.data);

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          new SubSectionHeader("Age"),
          new StatList(data["median_age"], "number"),
          new DistributionChart(data["age_category_distribution"],
              title: "Population by age category", pie: true),
          new DistributionChart(data["age_group_distribution"],
              title: "Population by 10-year age range"),
          new Divider(),
        ],
      ),
    );
  }
}
