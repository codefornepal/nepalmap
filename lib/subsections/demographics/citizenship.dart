import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class Citizenship extends StatelessWidget {
  final Map data;

  Citizenship(this.data);

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          new SubSectionHeader("Citizenship"),
          new DistributionChart(data["citizenship_distribution"], pie: true),
          new DistributionChart(
            data["citizenship_by_sex"],
            title: "Population by citizenship and sex",
            grouped: true,
          ),
          new Divider(),
        ],
      ),
    );
  }
}
