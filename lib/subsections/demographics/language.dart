import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class Language extends StatelessWidget {
  final Map data;

  Language(this.data);

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          new SubSectionHeader("Language"),
          new StatList(data["language_most_spoken"], "name",
              name: "Language most spoken at home"),
          new DistributionChart(data["language_distribution"],
              title: "Population by language most spoken at home"),
          new Divider(),
        ],
      ),
    );
  }
}
