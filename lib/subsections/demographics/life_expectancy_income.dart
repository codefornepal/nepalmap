import 'package:flutter/material.dart';

import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class LifeExpectancyAndIncome extends StatelessWidget {
  final Map data;

  LifeExpectancyAndIncome(this.data);

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          new SubSectionHeader("Life Expectancy and Income"),
          new StatList(data["life_expectancy"], "number"),
          new StatList(data["per_capita_income"], "dollar"),
          new Divider(),
        ],
      ),
    );
  }
}
