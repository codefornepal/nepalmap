import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class Livestock extends StatelessWidget {
  final Map data;

  Livestock(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new SubSectionHeader("Livestock"),
        new StatList(
          data["total_livestock"],
          "number",
        ),
        new DistributionChart(
          data["livestock_distribution"],
          title: "Livestock",
        ),
        new Divider(),
      ],
    );
  }
}
