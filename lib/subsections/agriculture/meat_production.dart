import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class MeatProduction extends StatelessWidget {
  final Map data;

  MeatProduction(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new SubSectionHeader("Meat Production"),
        new StatList(
          data["total_meat"],
          "number",
        ),
        new DistributionChart(
          data["meat_distribution"],
          title: "Metric tons of meat produced",
        ),
        new Divider(),
      ],
    );
  }
}
