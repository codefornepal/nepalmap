import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class EducationLevelBySex extends StatelessWidget {
  final Map data;

  EducationLevelBySex(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new SubSectionHeader("Education Level Passed by Sex"),
        new StatList(data["primary_level_reached_by_sex"], "percentage"),
        new DistributionChart(
          data["education_level_by_sex_distribution"],
          grouped: true,
          title: "Highest education level reached",
        ),
        new Divider(),
      ],
    );
  }
}
