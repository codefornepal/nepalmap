import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class Literacy extends StatelessWidget {
  final Map data;

  Literacy(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new SubSectionHeader("Literacy"),
        new DistributionChart(data["literacy_distribution"], pie: true),
        new DistributionChart(
          data["literacy_by_sex_distribution"],
          grouped: true,
          title: "Literacy rate by sex",
        ),
        new Divider(),
      ],
    );
  }
}
