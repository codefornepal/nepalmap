import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class SchoolAttendace extends StatelessWidget {
  final Map data;

  SchoolAttendace(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new SubSectionHeader("School Attendace"),
        new StatList(data["aged_five_to_twenty_five"], "number"),
        new DistributionChart(
          data["school_attendance_distribution"],
          pie: true,
          title: "School Attendace",
        ),
        new DistributionChart(
          data["school_attendance_by_sex_distribution"],
          grouped: true,
          columns: 3,
          title: "School attendance by sex",
        ),
        new Divider(),
      ],
    );
  }
}
