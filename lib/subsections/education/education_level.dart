import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class EducationLevel extends StatelessWidget {
  final Map data;

  EducationLevel(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new SubSectionHeader("Education Level Passed"),
        new StatList(data["primary_level_reached"], "percentage"),
        new DistributionChart(
          data["education_level_passed_distribution"],
          title: "Highest education level reached",
        ),
        new Divider(),
      ],
    );
  }
}
