import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class RegisterationDecade extends StatelessWidget {
  final Map data;

  RegisterationDecade(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new SubSectionHeader("Business registrations by decade"),
        new StatList(
          data["total_companies_registered"],
          "number",
        ),
        new DistributionChart(
          data["registered_company_decade_distribution"],
          title: "All company registrations",
        ),
        new DistributionChart(
          data["registered_private_company_decade_distribution"],
          title: "Private company registrations",
        ),
        new DistributionChart(
          data["registered_public_company_decade_distribution"],
          title: "Public company registrations",
        ),
        new DistributionChart(
          data["registered_foreign_company_decade_distribution"],
          title: "Foreign company registrations",
        ),
        new DistributionChart(
          data["registered_non_profit_decade_distribution"],
          title: "Non-profit registrations",
        ),
        new Divider(),
      ],
    );
  }
}
