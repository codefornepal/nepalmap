import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class CookingFuel extends StatelessWidget {
  final Map data;

  CookingFuel(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new SubSectionHeader("Main Type of Cooking Fuel"),
        new StatList(
          data["cooking_wood"],
          "percentage",
        ),
        new DistributionChart(
          data["cooking_fuel_distribution"],
          title: "Households by main type of cooking fuel",
        ),
        new Divider(),
      ],
    );
  }
}
