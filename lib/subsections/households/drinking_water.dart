import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class DrinkingWater extends StatelessWidget {
  final Map data;

  DrinkingWater(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new SubSectionHeader("Drinking Water Source "),
        new StatList(
          data["piped_tap"],
          "percentage",
        ),
        new DistributionChart(
          data["drinking_water_distribution"],
          title: "Households by drinking water source",
        ),
        new Divider(),
      ],
    );
  }
}
