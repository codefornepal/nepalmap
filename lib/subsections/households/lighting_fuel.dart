import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class LightingFuel extends StatelessWidget {
  final Map data;

  LightingFuel(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new SubSectionHeader("Main Type of Lighting Fuel"),
        new StatList(
          data["lighting_electricity"],
          "percentage",
        ),
        new DistributionChart(
          data["lighting_fuel_distribution"],
          title: "Households by main type of lighting fuel",
        ),
        new Divider(),
      ],
    );
  }
}
