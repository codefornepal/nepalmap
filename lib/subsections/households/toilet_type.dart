import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class ToiletType extends StatelessWidget {
  final Map data;

  ToiletType(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new SubSectionHeader("Household Toilet Type"),
        new StatList(
          data["flush_toilet"],
          "percentage",
        ),
        new DistributionChart(
          data["toilet_type_distribution"],
          title: "Households by toilet type",
        ),
        new Divider(),
      ],
    );
  }
}
