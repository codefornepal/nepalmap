import 'package:flutter/material.dart';

import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class HouseholdsData extends StatelessWidget {
  final Map data;

  HouseholdsData(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new SubSectionHeader("Households"),
        new StatList(data["total_households"], "number"),
        new StatList(data["average_household_size"], "number"),
        new StatList(data["male_to_female_ratio"], "number"),
        new Divider(),
      ],
    );
  }
}
