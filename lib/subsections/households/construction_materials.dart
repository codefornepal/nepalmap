import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class ConstructionMaterials extends StatelessWidget {
  final Map data;

  ConstructionMaterials(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new SubSectionHeader("Household Construction Material"),
        new DistributionChart(
          data["foundation_type_distribution"],
          pie: true,
          title: "Households by type of foundation",
        ),
        new DistributionChart(
          data["outer_wall_type_distribution"],
          pie: true,
          title: "Households by type of wall",
        ),
        new DistributionChart(
          data["roof_type_distribution"],
          pie: true,
          title: "Households by type of roof",
        ),
        new Divider(),
      ],
    );
  }
}
