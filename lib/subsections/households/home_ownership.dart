import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

class HomeOwnership extends StatelessWidget {
  final Map data;

  HomeOwnership(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        new SubSectionHeader("Home Ownership"),
        new StatList(
          data["own_home"],
          "percentage",
        ),
        new DistributionChart(
          data["home_ownership_distribution"],
          title: "Households by ownership",
        ),
        new Divider(),
      ],
    );
  }
}
