import 'dart:async';

import 'package:flutter/material.dart';
import 'package:NepalMap/codeMappings.dart';
import 'package:NepalMap/containers/profile.dart';
import 'package:NepalMap/components/customroute.dart';

class SearchUI extends StatefulWidget {
  _SearchUIState createState() => new _SearchUIState();
}

class _SearchUIState extends State<SearchUI> with TickerProviderStateMixin {
  TextEditingController _searchTextController;
  StreamController _streamController;
  Stream _onAdd;

  _SearchUIState() {
    _searchTextController = new TextEditingController();

    _streamController = new StreamController<List<Widget>>();
    _onAdd = _streamController.stream;
  }

  _showResult(String searchString) {
    int counter = 0;
    List<Widget> results = [];

    codeMappings.forEach((key, value) {
      if (counter >= 5) return;

      if (searchString.length > 2 &&
          value.toLowerCase().contains(searchString)) {
        counter++;
        results.add(
          new GestureDetector(
            onTap: () {
              Navigator.pop(context);

              Navigator.of(context).push(
                    new CustomRoute(
                      builder: (_) => new Profile(code: key, name: value),
                    ),
                  );
            },
            child: new Container(
              decoration: new BoxDecoration(
                border: new Border.all(color: Colors.white),
              ),
              padding:
                  new EdgeInsets.symmetric(horizontal: 5.0, vertical: 12.0),
              child: new Row(
                children: <Widget>[
                  new Text(
                    value,
                    style: new TextStyle(fontSize: 20.0),
                  ),
                  new Expanded(
                    child: new Align(
                      alignment: Alignment.centerRight,
                      child: new Text(
                        key.split("-")[0],
                        style: new TextStyle(
                          fontSize: 18.0,
                          fontStyle: FontStyle.italic,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      }
    });

    _streamController.add(results);
  }

  Widget build(BuildContext context) {
    return new Container(
      child: new Scaffold(
        backgroundColor: new Color(0x00000000),
        body: new Column(
          children: <Widget>[
            new Container(
              color: Colors.white,
              padding: new EdgeInsets.all(5.0),
              child: new Row(
                children: <Widget>[
                  new IconButton(
                    icon: new Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  new Expanded(
                    child: new TextField(
                      style: new TextStyle(fontSize: 18.0, color: Colors.black),
                      controller: _searchTextController,
                      onChanged: (value) {
                        _showResult(value.toLowerCase());
                      },
                      autofocus: true,
                      decoration: new InputDecoration.collapsed(
                        hintStyle: new TextStyle(fontSize: 18.0),
                        hintText: "Find a place in Nepal...",
                      ),
                    ),
                  ),
                ],
              ),
            ),
            new StreamBuilder(
              stream: _onAdd,
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return new Container();
                }

                return new AnimatedSize(
                  vsync: this,
                  duration: new Duration(milliseconds: 200),
                  child: new AnimatedContainer(
                    duration: new Duration(milliseconds: 100),
                    color: Colors.white,
                    child: new Column(
                      children: snapshot.data,
                    ),
                  ),
                );
              },
            ),
            new Expanded(
              child: new GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
