import 'package:flutter/material.dart';

class SubSectionHeader extends StatelessWidget {
  final String heading;
  SubSectionHeader(this.heading);

  @override
  Widget build(BuildContext context) {
    return new Align(
      alignment: Alignment.topLeft,
      child: new Container(
        width: MediaQuery.of(context).size.width,
        padding: new EdgeInsets.all(10.0),
        child: new Text(
          heading,
          style: new TextStyle(
            fontSize: 24.0,
          ),
        ),
      ),
    );
  }
}
