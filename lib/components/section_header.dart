import 'package:flutter/material.dart';

class SectionHeader extends StatelessWidget {
  final String heading;
  SectionHeader(this.heading);

  @override
  Widget build(BuildContext context) {
    return new Align(
      alignment: Alignment.topLeft,
      child: new Container(
        decoration: new BoxDecoration(
          color: new Color(0xFFEFF1E9),
          border: new Border(
            top: new BorderSide(
              width: 1.0,
              color: new Color(0xFFE8EADF),
            ),
            bottom: new BorderSide(
              width: 1.0,
              color: new Color(0xFFCAD0B5),
            ),
          ),
        ),
        width: MediaQuery.of(context).size.width,
        padding: new EdgeInsets.all(10.0),
        child: new Text(
          heading,
          style: new TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
