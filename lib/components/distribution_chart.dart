import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

List<charts.Color> colors = [
  new charts.Color(r: 224, g: 0, b: 21),
  new charts.Color(r: 0, g: 173, b: 239),
  new charts.Color(r: 98, g: 149, b: 204),
  new charts.Color(r: 221, g: 133, b: 192),
  new charts.Color(r: 142, g: 204, b: 35),
  new charts.Color(r: 252, g: 205, b: 6),
  new charts.Color(r: 219, g: 186, b: 151),
  new charts.Color(r: 170, g: 170, b: 170),
];

List<Color> normalColors = [
  new Color.fromRGBO(224, 0, 21, 1.0),
  new Color.fromRGBO(0, 173, 239, 1.0),
  new Color.fromRGBO(98, 149, 204, 1.0),
  new Color.fromRGBO(221, 133, 192, 1.0),
  new Color.fromRGBO(142, 204, 35, 1.0),
  new Color.fromRGBO(252, 205, 6, 1.0),
  new Color.fromRGBO(219, 186, 151, 1.0),
  new Color.fromRGBO(170, 170, 170, 1.0),
];

class Distribution {
  final String name;
  final double value;

  Distribution(this.name, this.value);
}

class DistributionChart extends StatelessWidget {
  final Map data;
  final String title;
  final bool pie;
  final bool grouped;
  final int columns;
  final type;

  DistributionChart(
    this.data, {
    this.title = "",
    this.pie = false,
    this.grouped = false,
    this.columns = 2,
    this.type = "percentage",
  });

  List<charts.Series<Distribution, String>> _generateSeriesList() {
    List listData = <Distribution>[];
    List groupData = <charts.Series<Distribution, String>>[];

    if (grouped) {
      data.forEach((key, value) {
        if (key != "metadata") {
          listData = <Distribution>[];
          value.forEach((key, value) {
            if (key != "metadata") {
              String groupName = value["name"];
              double distributionValue = value["values"]["this"];

              listData.add(new Distribution(groupName, distributionValue));
            }
          });

          groupData.add(
            new charts.Series<Distribution, String>(
              id: key,
              domainFn: (Distribution group, _) => group.name,
              measureFn: (Distribution group, _) => group.value,
              data: listData,
            ),
          );
        }
      });

      return groupData;
    } else {
      data.forEach((key, value) {
        if (key != "metadata") {
          String groupName = value["name"];
          double distributionValue = value["values"]["this"];

          listData.add(new Distribution(groupName, distributionValue));
        }
      });

      return [
        new charts.Series<Distribution, String>(
          id: 'Distribution',
          colorFn: (_, number) => pie
              ? colors[number % colors.length]
              : new charts.Color(r: 224, g: 0, b: 21),
          domainFn: (Distribution group, _) => group.name,
          measureFn: (Distribution group, _) => group.value,
          labelAccessorFn: (Distribution group, _) =>
              type == "percentage" ? '${group.value}%' : '${group.value}',
          data: listData,
        )
      ];
    }
  }

  List<List<String>> _generatePieLegend() {
    List legendData = <List<String>>[];

    data.forEach((key, value) {
      if (key != "metadata") {
        String groupName = value["name"];
        String distributionValue = value["values"]["this"].toString();
        if (type == "percentage") distributionValue = "$distributionValue%";

        legendData.add([groupName, distributionValue]);
      }
    });

    return legendData;
  }

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        title == ""
            ? new Container()
            : new Align(
                alignment: Alignment.topLeft,
                child: new Container(
                  margin: new EdgeInsets.all(10.0),
                  child: new Text(
                    title,
                    style: new TextStyle(fontSize: 20.0),
                  ),
                ),
              ),
        new Container(
          child: pie == true
              ? new Container(
                  child: Column(
                    children: <Widget>[
                      new Container(
                        height: MediaQuery.of(context).size.height / 3,
                        child: new charts.PieChart(
                          _generateSeriesList(),
                          defaultRenderer:
                              new charts.ArcRendererConfig(arcWidth: 40),
                          defaultInteractions: true,
                          animate: false,
                        ),
                      ),
                      new Container(child: new PieLegend(_generatePieLegend())),
                    ],
                  ),
                )
              : new Container(
                  child: grouped
                      ? new charts.BarChart(
                          _generateSeriesList(),
                          defaultRenderer: new charts.BarRendererConfig(
                            groupingType: charts.BarGroupingType.grouped,
                          ),
                          behaviors: [
                            new charts.SeriesLegend(
                              position: charts.BehaviorPosition.bottom,
                              desiredMaxColumns: columns,
                            )
                          ],
                          vertical: true,
                          animate: false,
                        )
                      : new charts.BarChart(
                          _generateSeriesList(),
                          barRendererDecorator:
                              new charts.BarLabelDecorator<String>(),
                          vertical: false,
                          animate: false,
                        ),
                  height: MediaQuery.of(context).size.height / 2,
                ),
          margin: new EdgeInsets.all(10.0),
        ),
      ],
    );
  }
}

class PieLegend extends StatelessWidget {
  final List<List<String>> data;
  PieLegend(this.data);

  _generateLegends() {
    List<LegendRect> legends = [];

    int index = 0;
    for (var item in data) {
      legends.add(new LegendRect(item[0], item[1], normalColors[index]));
      index++;
    }

    if (legends.length == 0)
      return [new Container()];
    else
      return legends;
  }

  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: _generateLegends(),
      ),
    );
  }
}

class LegendRect extends StatelessWidget {
  final String title;
  final String value;
  final Color color;

  LegendRect(this.title, this.value, this.color);

  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.symmetric(horizontal: 20.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Container(
            child: new Row(
              children: <Widget>[
                new Container(
                  height: 12.0,
                  width: 12.0,
                  color: color,
                ),
                new Container(width: 10.0),
                new Text(title),
              ],
            ),
          ),
          new Container(width: 10.0),
          new Expanded(
            child: new Container(
              height: 1.0,
              color: Colors.black12,
            ),
          ),
          new Container(width: 10.0),
          new Text(value),
        ],
      ),
    );
  }
}
