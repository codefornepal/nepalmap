import 'package:flutter/material.dart';

class ExpandableList extends StatelessWidget {
  final String title;
  final List<Widget> children;
  ExpandableList({@required this.title, @required this.children});

  Widget build(BuildContext context) {
    return new ExpansionTile(
      key: new PageStorageKey<String>(title),
      title: new Text(
        title,
        style: new TextStyle(fontSize: 20.0),
      ),
      initiallyExpanded: false,
      children: children,
    );
  }
}
