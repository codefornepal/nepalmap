import 'package:flutter/material.dart';
import 'package:NepalMap/containers/profile.dart';
import 'package:NepalMap/components/customroute.dart';

class InfoCard extends StatelessWidget {
  final Map data;
  final int population;

  InfoCard(this.data, this.population);

  List<Widget> _generateInfo(context) {
    String geoLevel = data["this"]["geo_level"];
    geoLevel = geoLevel.replaceFirst(geoLevel[0], geoLevel[0].toUpperCase());

    List<Widget> parents = [];

    Map parentsMap = data["parents"];
    int length = parentsMap.length;

    if (length > 0) {
      geoLevel = "$geoLevel in";
    }

    parentsMap.forEach((key, value) {
      parents.add(
        new GestureDetector(
          onTap: () {
            Navigator.of(context).push(
                  new CustomRoute(
                    builder: (_) => new Profile(
                          code: value["full_geoid"],
                          name: value["name"],
                        ),
                  ),
                );
          },
          child: new Text(
            value["name"],
            style: new TextStyle(
              color: Theme.of(context).primaryColor,
              decoration: TextDecoration.underline,
            ),
          ),
        ),
      );

      if (length > 1) {
        parents.add(
          new Text(","),
        );
        parents.add(new Container(width: 3.0));
      } else {
        parents.add(new Text("."));
      }
    });

    return [
      new Text(
        data["this"]["name"],
        style: new TextStyle(
          fontSize: 32.0,
          fontWeight: FontWeight.bold,
        ),
      ),
      new Container(
        child: new Row(
          children: <Widget>[
            new Text(geoLevel),
            new Container(width: 3.0),
            new Row(
              children: parents,
            )
          ],
        ),
      ),
      new Container(height: 20.0),
      population == null
          ? new Text("")
          : new Text(
              population.toString(),
              style: new TextStyle(
                fontSize: 32.0,
                fontWeight: FontWeight.bold,
              ),
            ),
      new Text(
        "Population",
        style: new TextStyle(
          fontSize: 18.0,
        ),
      ),
      new Container(height: 20.0),
      new Container(
        child: new Row(
          children: <Widget>[
            new Text(
              "Census data:",
              style: new TextStyle(fontWeight: FontWeight.bold),
            ),
            new Container(width: 3.0),
            new Text("2011"),
          ],
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: new EdgeInsets.all(10.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _generateInfo(context),
      ),
    );
  }
}
